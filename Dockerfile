FROM debian:stable
MAINTAINER Jean-Benoist Leger <jbleger@hds.utc.fr>

RUN \
    mkdir -p /usr/share/man/man1 /usr/share/man/man2 /usr/share/man/man3 /usr/share/man/man4 /usr/share/man/man5 /usr/share/man/man6 /usr/share/man/man7 /usr/share/man/man8 && \
    apt-get update && \
    apt-get install -y --no-install-recommends unzip python3-pip texlive-latex-recommended texlive-luatex texlive-xetex texlive-fonts-recommended cm-super-minimal texlive-fonts-extra dvipng pandoc wget webp curl make && \
    pip3 install --break-system-packages Pillow numpy scipy jupyter jupyter-cache flatlatex matplotlib && \
    apt-get --purge -y remove texlive.\*-doc$ && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* && \
    wget $(curl https://quarto.org/docs/download/_prerelease.json | grep -oP "(?<=\"download_url\":\s\")https.*${ARCH}\.deb") -O /tmp/quarto.deb && \
    dpkg -i /tmp/quarto.deb && \
    rm /tmp/quarto.deb && \
    pip3 cache purge && \
    rm -rf /tmp/* /var/tmp/* && \
    mktexlsr && \
    updmap-sys && \
    luaotfload-tool --update --force && \
    true
